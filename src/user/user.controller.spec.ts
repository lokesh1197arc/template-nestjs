import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from './user.controller';
import { UserService } from './user.service';

describe('UserController', () => {
  let controller: UserController;
  let service: UserService;
  let serviceMock: any;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [UserService],
    }).compile();

    controller = module.get<UserController>(UserController);
    service = module.get<UserService>(UserService);
    serviceMock = jest.spyOn(service, 'getDetails').mockImplementation(async () => { return 1; });
  });

  it('get all users', async () => {
    await expect(controller.getAll({ fullname: 'Test User', age: 25 })).resolves.toStrictEqual([{
      fullname: 'Test User', age: 25
    }]);
  });

  it('get user details by id', async () => {
    await expect(controller.getDetails(1)).resolves.toBe(1);
    expect(serviceMock).toHaveBeenCalledTimes(1);
    expect(serviceMock).toHaveBeenCalledWith(1);
  });
});
