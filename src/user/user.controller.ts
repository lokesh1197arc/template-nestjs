import { Controller, Logger, Get, Param } from '@nestjs/common';
import { User } from '../decorators/user.decorator';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  private logger: Logger;

  constructor(private service: UserService) {
    this.logger = new Logger('UserController');
  }

  @Get()
  async getAll(@User() user: any) {
    this.logger.verbose(user);
    return [user];
  }

  @Get('/:id')
  async getDetails(@Param('id') id: number) {
    return this.service.getDetails(id);
  }
}

