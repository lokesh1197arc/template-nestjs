import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as helmet from 'helmet';
import * as noCache from 'nocache';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(helmet.default());
  app.use(noCache());
  await app.listen(3000);
}
bootstrap();
