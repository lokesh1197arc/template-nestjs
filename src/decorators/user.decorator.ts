import { Logger, createParamDecorator, ExecutionContext, HttpException, HttpStatus } from '@nestjs/common';
import { UserDetails } from '../interfaces/user.interface';

export const User = createParamDecorator(
  async (data: unknown, ctx: ExecutionContext): Promise<UserDetails> => {
    const logger = new Logger('UserDecorator');
    try {
      const request = ctx.switchToHttp().getRequest();
      const headers = request.headers;

      logger.verbose('Headers: ', headers);
      logger.verbose('Data: ', data);
      return { fullname: 'Test User', age: 25 };
    } catch (err) {
      logger.error(err);
      throw new HttpException('Unauthorised', HttpStatus.UNAUTHORIZED);
    }
  },
);
